1
00:00:01,500 --> 00:00:09,000
This is an example subtitle file
of the popular Subrip (srt) format

2
00:00:09,500 --> 00:00:13,000
Any comments, suggestions and bug reports
regarding the package...

3
00:00:13,600 --> 00:00:17,400
...use reportbug

4
00:00:18,600 --> 00:00:21,600
or email to
submit@bugs.debian.org

5
00:00:22,600 --> 00:00:26,400
with a special format

6
00:00:26,800 --> 00:00:28,000
described at

7
00:00:28,600 --> 00:00:33,600
https://www.debian.org/Bugs/Reporting

8
00:00:34,000 --> 00:00:38,000
Have fun Subtitling!
